namespace TheOnlyDroid.Public.Integrated.Plugins
{
    public class MaplePluginWrapper
    {
        #region Fields
        private ITurbioPlugin plugin;
        private string name;
        private string description;
        private string author;
        private string contact;
        private string version;
        #endregion
        
        #region Attributees
        public string Name
        {
            get { return name; }
        }

        public string Description
        {
            get { return description; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Contact
        {
            get { return contact; }
        }

        public string Version
        {
            get { return version; }
        }
        #endregion

        /// <summary>
        /// Constructs a plugin wrapper
        /// </summary>
        public MaplePluginWrapper(IMaplePlugin plugin, string name, string description, string author, string contact, string version)
        {
            this.plugin = plugin;
            this.name = name;
            this.description = description;
            this.author = author;
            this.contact = contact;
            this.version = version;
        }

        /// <summary>
        /// Activate Plugin
        /// </summary>s
        public void Activate()
        {
            plugin.ActivatePlugin();
        }

        public void SafeDispose(object Sender)
        {
            plugin.SafeDispose(Sender);
        }
    }
}