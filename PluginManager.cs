namespace TheOnlyDroid.Public.Integrated.Plugins
{
    /* Place in a separate file if needed; I usually keep mine with all my other Exceptions
    public class PluginValidationError : Exception
    {
        public PluginValidationError(Type type, string error) : base(error)
        {
        }
    }
    */
    internal class MaplePluginManager
    {
        private List<MaplePluginWrapper> _loadedPlugins = new List<MaplePluginWrapper>();
        public List<MaplePluginWrapper> LoadedPlugins
        {
            get { return _loadedPlugins; }
        }

        internal void LoadPlugins()
        {
            string[] files = Directory.GetFiles("Plugins", "*.dll");

            foreach (string file in files)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFrom(file);
                    System.Type[] types = assembly.GetTypes();

                    foreach (System.Type type in types)
                    {
                        if (type.GetInterface("IMaplePlugin") != null)
                        {
                            if (type.GetCustomAttributes(typeof(MaplePluginDisplayNameAttribute), false).Length != 1)
                                throw new PluginValidationError(type, "[ERROR] Attribute 'MaplePluginDisplayNameAttribute' is not present/accessable... Please contact the developer");
                            if (type.GetCustomAttributes(typeof(MaplePluginAuthorAttribute), false).Length != 1)
                                throw new PluginValidationError(type, "[ERROR] Attribute 'MaplePluginAuthorAttribute' is not present/accessable... Please contact the developer");

                            string name = type.GetCustomAttributes(typeof(MaplePluginDisplayNameAttribute), false)[0].ToString();
                            string author = type.GetCustomAttributes(typeof(MaplePluginAuthorAttribute), false)[0].ToString();

                            string version = (type.GetCustomAttributes(typeof(MaplePluginVersionAttribute), false).Length != 1) ? "Not Provided" : type.GetCustomAttributes(typeof(MaplePluginVersionAttribute), false)[0].ToString();
                            string description = (type.GetCustomAttributes(typeof(MaplePluginDescriptionAttribute), false).Length != 1) ? "Not Provided" : type.GetCustomAttributes(typeof(MaplePluginDescriptionAttribute), false)[0].ToString();
                            string contact = (type.GetCustomAttributes(typeof(MaplePluginContactAttribute), false).Length != 1) ? "Not Provided" : type.GetCustomAttributes(typeof(MaplePluginContactAttribute), false)[0].ToString();

                            Object o = Activator.CreateInstance(type);
                            MaplePluginWrapper plugin = new MaplePluginWrapper(o as IMaplePlugin, name, description, author, contact, version);
                            _loadedPlugins.Add(plugin);
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, $"Plugin Exception [{file.ToString()}]", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}