namespace TheOnlyDroid.Public.Integrated.Plugins
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MaplePluginDisplayNameAttribute : System.Attribute
    {
        private string displayName;

        public MaplePluginDisplayNameAttribute(string displayName) : base ()
        {
            this.displayName = displayName;
        }

        public override string ToString()
        {
            return displayName;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MaplePluginDescriptionAttribute : System.Attribute
    {
        private string description;

        public MaplePluginDescriptionAttribute(string description) : base ()
        {
            this.description = description;
        }

        public override string ToString()
        {
            return description;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MaplePluginAuthorAttribute : System.Attribute
    {
        private string author;
        public MaplePluginAuthorAttribute(string author) : base ()
        {
            this.author = author;
        }

        public override string ToString()
        {
            return author;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MaplePluginVersionAttribute : System.Attribute
    {
        private string version;

        public MaplePluginVersionAttribute(string version) : base ()
        {
            this.version = version;
        }

        public override string ToString()
        {
            return version;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MaplePluginContactAttribute : System.Attribute
    {
        private string contact;

        public MaplePluginContactAttribute(string contact) : base()
        {
            this.contact = contact;
        }

        public override string ToString()
        {
            return contact;
        }
    }
}