namespace TheOnlyDroid.Public.Integrated.Plugins
{
    public interface IMaplePlugin
    {
        void ActivatePlugin();
        void SafeDispose(object Sender);
    }
}
